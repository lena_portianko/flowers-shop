class Card{

    constructor(){
        this.classNameActive = "to-cart_active";
        this.labelAdd = "В кошик";
        this.labelRemove = "Вилучити";
    }

    handleSetLocationStorage(el, article){
        var pushProduct = false;

        if(card[article] != undefined){
            delete card[article];
        }else{
            card[article]=1;
            pushProduct = true;
        }
        localStorage.setItem('card', JSON.stringify(card));

        if(pushProduct){
            el.classList.add(this.classNameActive);
            el.innerHTML = this.labelRemove;
        }else{
            el.classList.remove(this.classNameActive);
            el.innerHTML = this.labelAdd;
        }
    };

    render(){
        
        Catalog.forEach((element) => {
            if(element.article === sessionStorage.getItem("article")){
                            let topCatalog = '';
                            let buttonCatalog = '';
                            let aboutFlowerCatalog = '';
                            let activeClass = '';
                            let activeText = '';
            
                            if(card[element.article] == 1 || card[element.article] > 1){
                                activeClass = " " + this.classNameActive;
                                activeText = this.labelRemove;
                            }else{
                                activeText = this.labelAdd;
                            }

                        topCatalog += `
                            <div class="card-top">
                                <div class="card-img">
                                    <img src="${element.img}" alt="${element.name}" class="card_of_product"/>
                                </div>
                                <div class="card-sidebar">
                                    <div class="card-title">${element.name}</div>
                                    <p class="description">Артикул ${element.article}</p>
                                    <div class="card-price">
                                        <span>Ціна за 1 шт.</span> <b>${element.price}грн.</b>
                                        <span class="available">${element.availability}</span>
                                    </div>
                                    <div class="pay-block">
                                        <button class="to-cart${activeClass}" onclick="cardPage.handleSetLocationStorage(this, '${element.article}');">
                                            ${activeText}
                                        </button>
                                        <a href="./basket.html" class="card-add"></a>
                                    </div>
                                </div>
                            </div>
                        `;
                        buttonCatalog += `
                            <div class="card-bottom">
                                <div class="card-info">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#about_product">Про товар</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#product_parameters">Параметри</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link rev-swiper" data-toggle="tab" href="#product_reviews">Відгуки</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active container" id="about_product">
                                            <h2 class="tab">Опис</h2>
                                            <p>${element.description}</p>
                                        </div>
                                        <div class="tab-pane container" id="product_parameters">
                                            <ul class="product_ul">
                                                <li><b>Висота:</b> ${element.height}</li>
                                                <li><b>Діаметр:</b> ${element.diameter}</li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane container" id="product_reviews">
                                            <div class="slider-reviews">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide">
                                                        <div class="rev_header">
                                                            <div class="avatar">
                                                                <img src="./img/person-male.png" alt="person icon">
                                                            </div>
                                                            <div class="rev_avatar">
                                                                <div class="rev_name">Максим Петров</div>
                                                                <div class="rating">
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                </div>
                                                                <a href="" class="rev_social">anatoliypetrov@ukr.net</a>
                                                            </div>
                                                        </div>
                                                        <div class="rev_footer">
                                                            <p>
                                                                Великий вибір квітів, приємне місце, ввічливі продавці, які прислухаються до думки клієнта. 
                                                                Дають поради по догляду за квітами. Квіти завжди свіжі.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <div class="rev_header">
                                                            <div class="avatar">
                                                                <img src="./img/person-male.png" alt="person icon">
                                                            </div>
                                                            <div class="rev_avatar">
                                                                <div class="rev_name">Марія Стрижак</div>
                                                                <div class="rating">
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span></span>
                                                                </div>
                                                                <a href="" class="rev_social">mariya_stryzhak@gmail.com</a>
                                                            </div>
                                                        </div>
                                                        <div class="rev_footer">
                                                            <p>
                                                                Привітні продавці, завжди запропонують квіти відповідно до Ваших бажань. Все сподобалось, чудові квіти, великий вибір, але дорогувато.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide">
                                                        <div class="rev_header">
                                                            <div class="avatar">
                                                                <img src="./img/person-male.png" alt="person icon">
                                                            </div>
                                                            <div class="rev_avatar">
                                                                <div class="rev_name">Інна Демченко</div>
                                                                <div class="rating">
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                    <span class="active"></span>
                                                                </div>
                                                                <a href="" class="rev_social">demchenko.inna@meta.ua</a>
                                                            </div>
                                                        </div>
                                                        <div class="rev_footer">
                                                            <p>
                                                                Замовляла букет з доставкою, перебуваючи в іншому місті - все пройшло успішно, поставилися відповідально, букет іменинниці сподобався.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="swiper-button-next">
                                                    <span>Ще</span>
                                                    <img src="img/slide-next.png" alt="next slider">
                                                </div>
                                            </div>
                                            <div class="rev_button">
                                                <a href="#" data-toggle="modal" data-target='#reviews'>Додати відгук</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-sidebar">
                                    <div class="purchase-info">
                                        <div class="title">Особливості замовлення</div>
                                        <div class="line">
                                            <img src="img/upakovka.png" alt="upakovka">Оригінальна упаковка квітів</img>
                                        </div>
                                        <div class="line">
                                            <img src="img/shvidka_dostavka.png" alt="shvydko">Швидка доставка</img>
                                        </div>
                                        <div class="line">
                                            <img src="img/kachestvo.png" alt="garantiya">Гарантія якості</img>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                
                        aboutFlowerCatalog += `
                            ${topCatalog}
                            ${buttonCatalog}
                        `;

                    Root_Card_Flower.innerHTML = aboutFlowerCatalog;
                }
        });
    }
}

const cardPage = new Card();
cardPage.render();