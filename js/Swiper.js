var mySwiper = new Swiper('.swiper-container', {
    slidesPerView: 1,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
})

var menuSwiper = new Swiper('.swiper-container-menu', {
    slidesPerView: 5,
    spaceBetween: 30,
});

jQuery(function(){
    let j = jQuery;
    
    j('.change-label .label').on('click', function(){
        j(this).toggleClass('active');
        j('.change-label input').toggle();
    });

    j('.button-line button').on('click', function(){
        j('.button-line button').removeClass('active');
        j(this).addClass('active');
        j('.cart-sidebar form').toggle();
    });

    $('.rev-swiper').on('shown.bs.tab', function(e){
        var swiperReviews = new Swiper('.slider-reviews', {
            slidesPerView: 2,
            spaceBetween: 16,
            navigation: {
                nextEl: '.swiper-button-next'
            },
        });
    });

});