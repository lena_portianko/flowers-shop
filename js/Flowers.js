class Flowers {
      
        constructor(){
            this.classNameActive = "to-cart_active";
            this.labelAdd = "В кошик";
            this.labelRemove = "Вилучити";
            
        }
        
        handleSetLocationStorage(el, article){
            var pushProduct = false;

            if(card[article] != undefined){
                delete card[article];
            }else{
                card[article]=1;
                pushProduct = true;
            }
            localStorage.setItem('card', JSON.stringify(card));

            if(pushProduct){
                el.classList.add(this.classNameActive);
                el.innerHTML = this.labelRemove;
            }else{
                el.classList.remove(this.classNameActive);
                el.innerHTML = this.labelAdd;
            }
        };

        checkCard(){
            if(localStorage.getItem('card') != null){
                card = JSON.parse(localStorage.getItem('card'));
            }
        };

        render(){
            let htmlCatalog = '';
            
            Catalog.forEach((element) => {
                let activeClass = '';
                let activeText = '';
                
                if(card[element.article] == 1 || card[element.article] > 1){
                    activeClass = " " + this.classNameActive;
                    activeText = this.labelRemove;
                }else{
                    activeText = this.labelAdd;
                }

                htmlCatalog += `
                    <div class="product">
                        <div class="img">
                            <img src="${element.img}" alt="${element.name}">
                        </div>
                        <a href="./card.html" class="title" id="name_of_flower" onclick="sessionStorage.setItem('article', '${element.article}');">${element.name}</a>
                        <p class="description">Артикул ${element.article}</p>
                        <div class="price">
                            <b>${element.price}грн.</b>
                            <span class="available">${element.availability}</span>
                        </div>
                        <button class="to-cart${activeClass}" onclick="flowersPage.handleSetLocationStorage(this, '${element.article}');">
                            ${activeText} 
                        </button>
                        
                    </div>
                `;
            });

            Root_Product.innerHTML=htmlCatalog;
        }
    }
    var card = {};
    const flowersPage = new Flowers();
    flowersPage.checkCard();
    flowersPage.render();

    Root_Flowers.addEventListener("click", function(){
        const flowersPage = new Flowers();
        flowersPage.render();
        
        Root_Carnation.classList.remove("active");
        Root_Peony.classList.remove("active");
        Root_Rose.classList.remove("active");
        Root_Tulip.classList.remove("active");
        Root_Bouquet.classList.remove("active");
    });


    class TypeOfFlowers{
        constructor(){
            this.classNameActive = "to-cart_active";
            this.labelAdd = "В кошик";
            this.labelRemove = "Вилучити";
        }

        handleSetLocationStorage(el, article){
            var pushProduct = false;

            if(card[article] != undefined){
                delete card[article];
            }else{
                card[article]=1;
                pushProduct = true;
            }
            localStorage.setItem('card', JSON.stringify(card));

            if(pushProduct){
                el.classList.add(this.classNameActive);
                el.innerHTML = this.labelRemove;
            }else{
                el.classList.remove(this.classNameActive);
                el.innerHTML = this.labelAdd;
            }
            typeOfFlowersPage.render(typeFlow);

        };

        render(typeFlow){
            let flowerCatalog = '';

            Catalog.forEach((element)=> {
                let activeClass = '';
                let activeText = '';

                if(card[element.article] == 1 || card[element.article] > 1){
                    activeClass = " " + this.classNameActive;
                    activeText = this.labelRemove;
                }else{
                    activeText = this.labelAdd;
                }

                if(element.type == typeFlow){
                    flowerCatalog += `
                    <div class="product">
                        <div class="img">
                            <img src="${element.img}" alt="${element.name}">
                        </div>
                        <a href="./card.html" class="title" id="name_of_flower" onclick="sessionStorage.setItem('article', '${element.article}');">${element.name}</a>
                        <p class="description">Артикул ${element.article}</p>
                        <div class="price">
                            <b>${element.price}грн.</b>
                            <span class="available">${element.availability}</span>
                        </div>
                        <button class="to-cart${activeClass}" onclick="typeOfFlowersPage.handleSetLocationStorage(this, '${element.article}');">
                            ${activeText}
                        </button>
                        
                    </div>
                    `;

                    if(typeFlow == 'carnation'){
                        Root_Carnation.classList.add("active");
                        Root_Peony.classList.remove("active");
                        Root_Rose.classList.remove("active");
                        Root_Tulip.classList.remove("active");
                        Root_Bouquet.classList.remove("active");
                    }else if(typeFlow == 'peony'){
                        Root_Carnation.classList.remove("active");
                        Root_Peony.classList.add("active");
                        Root_Rose.classList.remove("active");
                        Root_Tulip.classList.remove("active");
                        Root_Bouquet.classList.remove("active");
                    }else if(typeFlow == 'rose'){
                        Root_Carnation.classList.remove("active");
                        Root_Peony.classList.remove("active");
                        Root_Rose.classList.add("active");
                        Root_Tulip.classList.remove("active");
                        Root_Bouquet.classList.remove("active");
                    }else if(typeFlow == 'tulip'){
                        Root_Carnation.classList.remove("active");
                        Root_Peony.classList.remove("active");
                        Root_Rose.classList.remove("active");
                        Root_Tulip.classList.add("active");
                        Root_Bouquet.classList.remove("active");
                    }else if(typeFlow == 'bouquet'){
                        Root_Carnation.classList.remove("active");
                        Root_Peony.classList.remove("active");
                        Root_Rose.classList.remove("active");
                        Root_Tulip.classList.remove("active");
                        Root_Bouquet.classList.add("active");
                    }
                }
                
            });

            Root_Product.innerHTML =flowerCatalog;
        }
    }

    const typeOfFlowersPage = new TypeOfFlowers();

    Root_Carnation.addEventListener("click", function(){
        const typeFlow = "carnation";
        typeOfFlowersPage.render(typeFlow);
    });

    Root_Peony.addEventListener("click", function(){
        const typeFlow = "peony";
        typeOfFlowersPage.render(typeFlow);
    });

    Root_Rose.addEventListener("click", function(){
        const typeFlow = "rose";
        typeOfFlowersPage.render(typeFlow);
    });

    Root_Tulip.addEventListener("click", function(){
        const typeFlow = "tulip";
        typeOfFlowersPage.render(typeFlow);
    });

    Root_Bouquet.addEventListener("click", function(){
        const typeFlow = "bouquet";
        typeOfFlowersPage.render(typeFlow);
    });
