class Shopping {

    checkCard(){
        if(localStorage.getItem('card') != null){
            card = JSON.parse(localStorage.getItem('card'));
        }
    };

    removeCard(art){
        delete card[art];
        localStorage.setItem('card', JSON.stringify(card));
        shoppingPage.render();
    };

    minusKilk(art){
        card[art];
        if(card[art]==1){
            delete card[art];
            localStorage.setItem('card', JSON.stringify(card));
            shoppingPage.render();
        }else{
            card[art]--;
            localStorage.setItem('card', JSON.stringify(card));
            shoppingPage.render();
        }
    };

    plusKilk(art){
        card[art]++;
        localStorage.setItem('card', JSON.stringify(card));
        shoppingPage.render();
    };

    render(){
        let htmlCatalog = '';
        let sumCatalog = 0;

        Catalog.forEach((element)=> {
            if(card[element.article] == 1 || card[element.article] > 1){
                htmlCatalog += `
                    <div class="cart-product-item">
                        <div class="cart-product-left d-flex">
                            <img class="cart-product-remove" id="remove-btn" src="img/cart-remove.png" alt="remove" onclick="shoppingPage.removeCard('${element.article}');">
                            <div class="cart-product-img">
                                <img src="${element.img}" alt="${element.name}">
                            </div>
                            <div class="cart-product-name">${element.name}</div>
                        </div>
                        <div class="cart-product-right d-flex">
                            <div class="cart-product-toggle">
                                <div class="input-range">
                                    <span class="minus" onclick="shoppingPage.minusKilk('${element.article}');">-</span>
                                    <span id="kilkist">${card[element.article]}</span>
                                    <span class="plus" onclick="shoppingPage.plusKilk('${element.article}');">+</span>
                                </div>
                            </div>
                            <div class="cart-product-prise" id="cart-product-prise">${card[element.article]*element.price} грн.</div>
                        </div>
                    </div>
                `;
                sumCatalog += card[element.article]*element.price;
            }
        });

        Root_Shopping.innerHTML = htmlCatalog;
        Root_Sum_Tog.innerHTML = sumCatalog + " грн.";
    }
}

var card = {};
const shoppingPage = new Shopping();
shoppingPage.checkCard();
shoppingPage.render();

