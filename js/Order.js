class Order {

    checkCard(){
        if(localStorage.getItem('card') != null){
            card = JSON.parse(localStorage.getItem('card'));
        }
    };

    calcOrder(){
        let productFlowers;
        if(localStorage.getItem('card') != null){
            productFlowers =localStorage.getItem('card');
        }
        Root_Card_Product_Flowers.value = productFlowers;
        Root_Card_Tovar_Flowers.value = productFlowers;
    };

    render(){
        let htmlCatalog = '';
        let sumCatalog = 0;
        let deliveryCatalog = 0;

        Catalog.forEach((element)=> {
            if(card[element.article] == 1 || card[element.article] > 1){
                htmlCatalog += `
                    <div class="cart-product-item">
                        <div class="cart-product-left d-flex">
                            <div class="cart-product-img">
                                <img src="${element.img}" alt="${element.name}">
                            </div>
                            <div class="cart-product-name">${element.name}</div>
                        </div>
                        <div class="cart-product-right d-flex">
                            <div class="cart-product-toggle">
                            </div>
                            <div class="cart-product-prise" id="cart-product-prise">${card[element.article]*element.price} грн.</div>
                        </div>
                    </div>
                `;
                sumCatalog += card[element.article]*element.price;
            }
        });

        Root_Order.innerHTML = htmlCatalog;
        Root_Sum_Tog.innerHTML = sumCatalog;
        Root_Sum_Del.innerHTML = deliveryCatalog;
        Root_Sum_Pay.innerHTML = (sumCatalog + deliveryCatalog);
        
        Root_Summ_Of_Tovar.value = sumCatalog;
        Root_Together_Summ.value = Root_Sum_Pay.innerHTML;
        
        
            Root_City_Delivery.addEventListener("click", function(){
    
                let city_del = Root_City_Delivery.options[Root_City_Delivery.selectedIndex].value;
                if(city_del == "Київ"){
                    deliveryCatalog = 0;
                }else if(city_del == "місто в Київській області"){
                    deliveryCatalog = 200;
                }else if(city_del == "інше місто України"){
                    deliveryCatalog = 450;
                }else{
                    deliveryCatalog = 0;
                }
                Root_Sum_Del.innerHTML = (deliveryCatalog + sum_time);
                Root_Sum_Pay.innerHTML = (sumCatalog + +Root_Sum_Del.innerHTML);
            });
            
            let sum_time = 0;

            Root_Time_Delivery.addEventListener("click", function(){
                let time_del = Root_Time_Delivery.options[Root_Time_Delivery.selectedIndex].value;
                if(time_del == "з 09:00 до 14:00" || time_del == "з 14:00 до 18:00" || time_del == "з 18:00 до 21:00"){
                    sum_time = 0;
                }else if(time_del == "з 21:00 до 08:59"){
                    sum_time = 200;
                }else{
                    sum_time = 130;
                }
                Root_Sum_Del.innerHTML = (deliveryCatalog + sum_time);
                Root_Sum_Pay.innerHTML = (sumCatalog + +Root_Sum_Del.innerHTML);
            });
            
        
        Root_Form_Two.addEventListener("click", function(){
            deliveryCatalog = 0; 
            sum_time = 0;
            Root_Sum_Tog.innerHTML = sumCatalog;
            Root_Sum_Del.innerHTML = deliveryCatalog;
            Root_Sum_Pay.innerHTML = (sumCatalog + deliveryCatalog);
            
            Root_Summ_Of_Product.value = sumCatalog;
            Root_Together_Summs.value = Root_Sum_Pay.innerHTML;
        });


        Root_Sender.addEventListener("submit", function(){
            localStorage.clear();        
        });

        Root_Sender_Self_Pickup.addEventListener("submit", function(){
            localStorage.clear();        
        });

        
    }
}


var card = {};
const orderPage = new Order();
orderPage.checkCard();
orderPage.calcOrder();
orderPage.render();


