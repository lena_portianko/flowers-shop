const Root_Product = document.getElementById("all-products");
const Root_Flowers = document.getElementById("logotype");

const Root_Carnation = document.getElementById("type_carnation");
const Root_Peony = document.getElementById("type_peony");
const Root_Rose = document.getElementById("type_rose");
const Root_Tulip = document.getElementById("type_tulip");
const Root_Bouquet = document.getElementById("type_buoquet");

const Root_Basket = document.getElementById("basket");
const Root_Shopping = document.getElementById("cart-product-items");
const Root_Number = document.getElementById("number_of_flower");

const Root_Card_Flower = document.getElementById("card-flower");
const Root_Count_Flowers = document.getElementById("sum");
const Root_Kilkist = document.getElementById("kilkist");
const Root_Price = document.getElementById("cart-product-prise");
const Root_Tovar = document.getElementById("new_tovar");

const Root_Zakaz = document.getElementById("cart-btn");
const Root_Order = document.getElementById("order-product-items");
const Root_Sum_Tog = document.getElementById("sum_together");
const Root_Sum_Del = document.getElementById("sum_delivery");
const Root_Sum_Pay = document.getElementById("sum_to_pay");
const Root_City_Delivery = document.getElementById("city_of_delivery");
const Root_Time_Delivery = document.getElementById("time_of_delivery");

const Root_Form_One = document.getElementById("form_one");
const Root_Form_Two = document.getElementById("form_two");

const Root_Sender = document.getElementById("sender");

const Root_Sender_Self_Pickup = document.getElementById("senderSelfPickup");

const Root_Card_Tovar_Flowers = document.getElementById("tovars_flowers");
const Root_Summ_Of_Tovar = document.getElementById("summ_of_tovar");
const Root_Together_Summ = document.getElementById("together_summ");

const Root_Card_Product_Flowers = document.getElementById("products_flowers");
const Root_Summ_Of_Product = document.getElementById("summ_of_product");
const Root_Together_Summs = document.getElementById("together_summs");
